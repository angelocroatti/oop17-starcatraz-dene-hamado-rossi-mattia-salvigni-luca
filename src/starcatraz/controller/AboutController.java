package starcatraz.controller;

/**
 * Credits controller.
 */
public interface AboutController {

    /**
     * Close CreditsView.
     */
    void closeCreditsButtonClick();
}
