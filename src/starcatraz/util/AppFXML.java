package starcatraz.util;

/**
 * This class is used to store every path to an fxml file in the project.
 */
public final class AppFXML {

    public static final String ABOUT_VIEW = "/starcatraz/view/AboutView.fxml";
    public static final String ACHIEVEMENTS_VIEW = "/starcatraz/view/AchievementsView.fxml";
    public static final String MANUAL_VIEW = "/starcatraz/view/ManualView.fxml";
    public static final String MENU_VIEW = "/starcatraz/view/MenuView.fxml";
    public static final String PAUSE_VIEW = "/starcatraz/view/PauseView.fxml";
    public static final String ROOT_LAYOUT = "/starcatraz/view/RootLayout.fxml";
    public static final String SETTINGS_VIEW = "/starcatraz/view/SettingsView.fxml";
    public static final String STATISTICS_VIEW = "/starcatraz/view/StatisticsView.fxml";
    public static final String CHIP_USAGE_VIEW = "/starcatraz/view/game/ChipUsageView.fxml";
    public static final String DEFEAT_VIEW = "/starcatraz/view/game/DefeatView.fxml";
    public static final String GAME_VIEW = "/starcatraz/view/game/GameView.fxml";
    public static final String ROBOT_ATTACK_VIEW = "/starcatraz/view/game/RobotAttackView.fxml";
    public static final String ROCKET_ACTIVATION_VIEW = "/starcatraz/view/game/RocketActivationView.fxml";
    public static final String SELECT_CARD_VIEW = "/starcatraz/view/game/SelectCardView.fxml";
    public static final String VICTORY_VIEW = "/starcatraz/view/game/VictoryView.fxml";
    public static final String MANUAL_IT = "/fxml/manual_IT.fxml";
    public static final String MANUAL_EN = "/fxml/manual_EN.fxml";
    public static final String CREDITS = "/fxml/credits.fxml";

    private AppFXML() { }
}
